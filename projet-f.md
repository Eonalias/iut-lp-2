#Pokedex 1G

Créez un *Pokédex* avec les 151 premiers Pokémons : grille css Bootstrap, lazyloading, filtrage par type et modal de détail de pokemon seront vos objectifs !

---

Vous devrez travailler en groupe à l'aide de `GIT` et `npm`. Pensez bien à l'organisation du projet et à communiquer.
Pour rappel, certains fichiers sont dispensables, il faut qu'ils soient ignorés (exemple : node_modules).

N'utilisez ni CDN, ni téléchargement manuel, à l'exception des images.

Chaque projet doit avoir un pied de page avec un zone d'inscription à une newsletter. Affichez un message de confirmation lors de l'inscription en utilisant [tingle.js](https://robinparisi.github.io/tingle/). 

Enfin, une série de plugins sont à intégrer **obligatoirement** pour ce projet : 

##Plugins
- [Lazysizes](https://afarkas.github.io/lazysizes/index.html)
- [Select2](https://select2.org/selections) : Pour selectionner les types
- [jQuery QuickSand](https://razorjack.net/quicksand/) : Pour filtrer les Pokémons en fonction du/des type(s) sélectionnés.
- [Tingle.js](https://robinparisi.github.io/tingle/), pour afficher une modal descriptive du pokémon cliqué. 

## Liens utiles
- [Sprites](https://pokemondb.net/sprites#gen1)
- [Package Pokedex](https://github.com/contolini/pokedex/tree/master/scripts) 
- [Exemple de pokedex](https://pokedex.org)
