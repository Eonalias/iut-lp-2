# Projet web | licence

## Consignes initiales

- Initiez un nouveau dépot GIT "evaluation-nom-prenom", puis ouvrez celui-ci en local.
- À l'aide de `npm`, `GIT` réaliser le projet suivant puis une fois terminé : `pusher`.

## Consignes Projet d'évaluation
 
 À l'aide de `LESS`, `jQuery`, `Bootstrap@3` et [Bootstrap ColorPicker v2.x](https://github.com/farbelous/bootstrap-colorpicker/tree/v2.x) ([docs](https://farbelous.io/bootstrap-colorpicker/v2/)), afficher le colorpicker sur la page en inline mode.
La couleur selectionnée doit s'appliquer au background-color de la page (avec transition css). Bien entendu, vos assets de production doivents être minifiés et concaténés. Le dépot local et celui du remote doivent être au même stage.

## Quelques commandes NPM

- `npm init` ou `npm init --yes` ou `npm init -y`
- `npm install` ou `npm i`
- `npm i nom-du-package`
- `npm i nom-du-package --save-prod` ou `npm i nom-du-package -P`
- `npm i nom-du-package --save-dev` ou `npm i nom-du-package -D`
- `npm remove nom-du-package` ou `npm r nom-du-package` 
- `npm run nom-du-script` Execute un script déclarer dans le fichier package.json

## Quelques commandes GIT

Client Git gratuit et cross-plateform : [SourceTree](https://www.sourcetreeapp.com/)

- `git status`
- `git add path/du/fichier.ext`
- `git add "test/*.ext"` : ajoute l'ensemble des fichiers *.ext du dossier **test** 
- `git commit` : sans le paramètre `-m` un éditeur VIM s'ouvre :
	* `i` pour le mode insertion
	* `:wq` pour quitter
- `git commit -m "nom de votre commit"`
- `git commit --all -m "nom de votre commit"` : ajoute l'ensemble des fichiers modifiés automatiquement.
- `git log --graph` : Pour voir le graph
- `git log --all --decorate --oneline --graph` : Comme la commande précedente, en plus compact.
- `git help COMMAND` : ouvre la documentation sur une commande git dans votre navigateur.
