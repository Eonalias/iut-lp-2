#Festival HelloBirds : Saint Malo

Réalisez une page très graphique pour présenter le festival édition 2019. Vous devrez composer avec les éléments suivant : un Hero avec un background Video, une grille d'image, une visionneuse d'image, un compte à rebours et de visuels en parallax.

---

Vous devrez travailler en groupe à l'aide de `GIT` et `npm`. Pensez bien à l'organisation du projet et à communiquer.
Pour rappel, certains fichiers sont dispensables, il faut qu'ils soient ignorés (exemple : node_modules).

N'utilisez ni CDN, ni téléchargement manuel, à l'exception des images.

Chaque projet doit avoir un pied de page avec un zone d'inscription à une newsletter. Affichez un message de confirmation lors de l'inscription en utilisant [tingle.js](https://robinparisi.github.io/tingle/). 

Enfin, une série de plugins sont à intégrer **obligatoirement** pour ce projet : 

##Plugins
- [jQuery CountDown](http://hilios.github.io/jQuery.countdown/)
- [Parallax.js](https://www.npmjs.com/package/parallax.js)
- [jquery-youtube-background](https://www.npmjs.com/package/jquery-youtube-background)
- [LightGallery (version Jquery)](http://sachinchoolur.github.io/lightGallery/)