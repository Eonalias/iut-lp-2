#Star Citizen

Réalisez une page d'attente pour la date de sortie du jeu vidéo *StarCitizen* avec un décompte live.
En dessous, affichez un mur de visuel dans le style 'Pinterest' à l'aide du plugin Masonry.

---

Vous devrez travailler en groupe à l'aide de `GIT` et `npm`. Pensez bien à l'organisation du projet et à communiquer.
Pour rappel, certains fichiers sont dispensables, il faut qu'ils soient ignorés (exemple : node_modules).

N'utilisez ni CDN, ni téléchargement manuel, à l'exception des images.

Chaque projet doit avoir un pied de page avec un zone d'inscription à une newsletter. Affichez un message de confirmation lors de l'inscription en utilisant [tingle.js](https://robinparisi.github.io/tingle/). 

Enfin, une série de plugins sont à intégrer **obligatoirement** pour ce projet : 

- [jQuery CountDown](http://hilios.github.io/jQuery.countdown/)
- [Masonry-layout](https://www.npmjs.com/package/masonry-layout)
- [LightGallery (version Jquery)](http://sachinchoolur.github.io/lightGallery/)