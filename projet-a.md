#Escape Game Custom AKA EGC

Réaliser la page d'accueil du site de la société Escape Game Custom. Celle-ci est spécialisée dans la réalisation d'escape game événementiel (anniversaire, salon, festivals, etc.).
Le site doit proposer un double datePicker proposant à l'internaute de sélectionner une période (minimum deux jours consécutifs) pour l'évènement.

---

Vous devrez travailler en groupe à l'aide de `GIT` et `npm`. Pensez bien à l'organisation du projet et à communiquer.
Pour rappel, certains fichiers sont dispensables, il faut qu'ils soient ignorés (exemple : node_modules).

N'utilisez ni CDN, ni téléchargement manuel, à l'exception des images.

Chaque projet doit avoir un pied de page avec un zone d'inscription à une newsletter. Affichez un message de confirmation lors de l'inscription en utilisant [tingle.js](https://robinparisi.github.io/tingle/). 

Enfin, une série de plugins sont à intégrer **obligatoirement** pour ce projet : 

##Plugins
- [bootstrap-datepicker](https://github.com/uxsolutions/bootstrap-datepicker)
- [LightGallery (Jquery)](http://sachinchoolur.github.io/lightGallery/) : Visionneuse d'image à appliquer sur un carousel d'images 'Nos réalisations'.
- [Owl Carousel 2](https://github.com/OwlCarousel2/OwlCarousel2) :  Pour le carousel 'Nos réalisations'
- [Fontawesom@4](https://fontawesome.com/v4.7.0/get-started/)